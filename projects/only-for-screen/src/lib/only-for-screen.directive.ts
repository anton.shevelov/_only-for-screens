import {
    Directive,
    TemplateRef,
    ViewContainerRef,
    Input,
    OnInit,
    OnDestroy,
} from '@angular/core';
import { fromEvent, Subject } from 'rxjs';
import { throttleTime, distinctUntilChanged } from 'rxjs/operators';
@Directive({
    // tslint:disable-next-line: directive-selector
    selector: '[onlyForScreen]',
    providers: [{ provide: Window, useValue: window }],
})
export class OnlyForScreenDirective implements OnInit, OnDestroy {
    @Input() onlyForScreen;
    private resize$ = fromEvent(window, 'resize');
    private unsubscribe$ = new Subject<boolean>();
    private MOBILE = 641;
    private TABLET = 1025;

    constructor(
        private templateRef: TemplateRef<any>,
        private viewContainer: ViewContainerRef,
    ) {
        this.resize$
            .pipe(throttleTime(250), distinctUntilChanged())
            .subscribe(() => {
                this.updateOnResize();
            });
    }

    ngOnInit(): void {
        this.updateOnResize();
    }
    actualScreenDimention(): string {
        if (window.innerWidth < this.MOBILE) {
            return 'mobile';
        }
        if (window.innerWidth < this.TABLET) {
            return 'tablet';
        }
        return 'desktop';
    }

    updateOnResize() {
        this.viewContainer.clear();
        if (this.actualScreenDimention().includes(this.onlyForScreen)) {
            this.viewContainer.createEmbeddedView(this.templateRef);
        }
    }
    ngOnDestroy(): void {
        this.unsubscribe$.next(true);
        this.unsubscribe$.complete();
    }
}
