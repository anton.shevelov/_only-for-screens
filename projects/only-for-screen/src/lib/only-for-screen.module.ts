import { NgModule } from '@angular/core';
import { OnlyForScreenDirective } from './only-for-screen.directive';

@NgModule({
    declarations: [OnlyForScreenDirective],
    imports: [],
    exports: [OnlyForScreenDirective],
})
export class OnlyForScreenModule {}
